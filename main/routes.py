from flask import render_template, request, Blueprint, flash, make_response, g
from app.posts.forms import IndexcalendarForm as ChoosecalendarForm
from app.models import Post
from app.users.forms import RegistrationForm, ContactForm
from app.models import User
from app import bcrypt, db
from config import Config
# Высылка почты при регистрации мне и новому клиенту. Сохранение картинок которые добавляет новый пользователь
from app.users.utils import send_registration_request, save_request_pic
# Сохранение картинок для отправки мне на почту
from app.users.utils import save_request_forsend_pic32, save_request_forsend_pic31, save_request_forsend_residence
from datetime import datetime, timedelta

main = Blueprint('main', __name__)


# Домашняя страница
@main.route('/', methods=['POST', 'GET'])
@main.route('/home', methods=['POST', 'GET'])
def home():
    currentdate = datetime.now().strftime("%Y-%m-%d")
    mindateto = datetime.now() + timedelta(days=1)
    mindateto = mindateto.strftime("%Y-%m-%d")
    maxavailabledate = datetime.now() + timedelta(days=366)
    maxavailabledate = maxavailabledate.strftime("%Y-%m-%d")
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)
    form = ChoosecalendarForm()
    if form.validate_on_submit():
        receivedatefrom = form.datefrom.data
        receivedateto = form.dateto.data
        if receivedatefrom >= receivedateto:
            flash('ОШИБКА! Дата начала аренды не может быть больше даты окончания аренды!')
            return render_template('index.html', form=form, posts=posts, currentdatefrom=currentdate,         currentdateto=mindateto, maxavailabledate=maxavailabledate)
        else:
            posts = Post.query.order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)
            postsx = Post.query.order_by(Post.date_posted.desc()).all()

            #Выводим список дат которые ввёл пользователь
            date_generated = [receivedatefrom + timedelta(days=x)
            for x in range(0, (receivedateto-receivedatefrom).days)]
            check_the_data = str()
            for date in date_generated:
                check_the_data += date.strftime("%Y-%m-%d,")
            print("")
            print("Нужные даты: ", check_the_data)
            print("Тип нужных дат: ", type(check_the_data))
            print("")

            # allposts - содержит в себе id's всех доступных постов
            allposts = list()
            for y in postsx:
                allposts.append(y.id)
            print("Все объявления: ", allposts )
            print("Тип всех объявлений: ", type(allposts))
            print("")

            # exclude содержит в себе id's которые нужно удалить из allposts
            readyposts = Post.query.filter(Post.notavailable.contains(check_the_data)).all()

            print("Посты, которые нельзя выводить: ", readyposts)
            print("Тип постов, которые нельзя выводить: ", type(readyposts))
            print("")

            #Посты, которые нужно вывести на экран
            result = list()
            for z in readyposts:
                print(z.id)
                result.append(z.id)
            print(result)
            toshow=list(set(allposts) ^ set(result))
            print("A toshow is: ", toshow)
            print("A type is: ", type(toshow))

            #Вывод на экран постов, которые нужно вывести на

            showme = Post.query.filter(Post.id.in_(toshow)).order_by(Post.date_posted.desc()).\
            paginate(page=page, per_page=5)
            print("XXX: ", showme)
            return render_template('index.html', form=form, posts=showme,
                               currentdatefrom=receivedatefrom,
                               currentdateto=receivedateto, currentdateset=currentdate, maxavailabledate=maxavailabledate)
    return render_template('index.html', form=form, posts=posts, currentdatefrom=currentdate,           currentdateto=mindateto, maxavailabledate=maxavailabledate)


# Админ панель владельца, доступ возможен только мне
@main.route('/god', methods=['POST', 'GET'])
def godishere():
    auth = request.authorization
    if auth and request.authorization.username == Config.GOD_USERNAME and request.authorization.password == Config.GOD_PASS:
        x = str()
        f = open('auth_list.xrpt')
        for line in f:
            x += line
        f.close()
        form = RegistrationForm()
        if form.validate_on_submit():
            hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
            user = User(username=form.username.data, email=form.email.data, password=hashed_password, name = form.name.data, surname = form.surname.data, phone = form.phone.data)
            db.session.add(user)
            db.session.commit()
            flash(f'Аккаунт создан для {form.username.data}', 'success')
            form.username.data = ""
            form.password.data = ""
            form.email.data = ""
            form.name.data = ""
            form.surname.data = ""
            form.phone.data = ""
        return render_template('god.html', form=form, x=x)
    else:
        return make_response('', 401, {'www-Authenticate': 'Basic realm = "Login Required"'})


# Разлогинивание владельца (меня)
@main.route('/godout', methods=['POST', 'GET'])
def godout():
    return make_response('', 401, {'www-Authenticate': 'Basic realm = "Login Required"'})


# Как пользоваться сайтом? (extends base.html)
@main.route('/useit')
def useit():
    return render_template('useit.html', title='Как пользоваться сайтом?')


# Как стать партнером? (extends base.html)
@main.route('/partner', methods=['POST', 'GET'])
def partner():
    form = ContactForm()
    if form.validate_on_submit():
        save_request_pic(form.cont_name.data, form.cont_surname.data, form.cont_picture32.data)
        save_request_forsend_pic32(form.cont_picture32.data)
        save_request_pic(form.cont_name.data, form.cont_surname.data, form.cont_picture31.data)
        save_request_forsend_pic31(form.cont_picture31.data)
        save_request_pic(form.cont_name.data, form.cont_surname.data, form.cont_picture_residence.data)
        save_request_forsend_residence(form.cont_picture_residence.data)
        send_registration_request(form.cont_name.data, form.cont_surname.data, form.cont_phone.data,
                                  form.cont_datas.data, form.cont_email.data)
        flash(f'Заявка успешно отправлена! Ожидайте звонка в течение 48 часов.', 'success')
        form.cont_name.data = ''
        form.cont_surname.data = ''
        form.cont_phone.data = ''
        form.cont_datas.data = ''
        form.cont_email.data = ''
        return render_template('partner.html', form=form, title='Как стать партнером?')
    else:
        return render_template('partner.html', form=form, title='Как стать партнером?')


# О компании (extends base.html)
@main.route('/about')
def about():
    return render_template('about.html', title='О кампании')
