import os
import secrets
from PIL import Image
from flask import url_for, current_app
from flask_mail import Message
from app import mail
from flask_login import current_user
from app import create_app


# Функция сохранения картинки когда пользователь обновляет ее из кабинета
def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + str('_') + str(current_user.username) + f_ext
    picture_path = os.path.join(current_app.root_path, 'static/profile pictures', picture_fn)
    # output_size = (100, 100)
    i = Image.open(form_picture)
    # i.thumbnail(output_size)
    i.save(picture_path)
    return picture_fn


def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Восстановление пароля',
                  sender='noreply@demo.com',
                  recipients=[user.email])
    msg.body = f'''Для восстановления пароля, перейдите по ссылке:
{url_for('users.reset_token', token=token, _external=True)}
Если вы не делали запрос на восстановление пароля - проигнорируйте это сообщение.
'''
    mail.send(msg)


# Отправка сообщений на почту контрагента и на мою
def send_registration_request(name, surname, phone, data, email):
    msg = Message('Запрос сотрудничества',
                  sender='noreply@demo.com',
                  recipients=['neverleash@gmail.com'])
    msg.body = f'''Поздравляю! С нами снова хотят иметь дело.
    Имя:     {(name)}
    Фамилия: {(surname)}
    Телефон: {(phone)}
    Инфа:    {(data)}
    Почта:   {(email)}
    '''
    with create_app().open_resource('static/request_partnership_photos/page32.jpg') as fp:
        msg.attach("page32.png", "image/png", fp.read())
    with create_app().open_resource('static/request_partnership_photos/page31.jpg') as fp:
        msg.attach("page31.png", "image/png", fp.read())
    with create_app().open_resource('static/request_partnership_photos/residence.jpg') as fp:
        msg.attach("residence.png", "image/png", fp.read())
    mail.send(msg)

    msg_to_partner = Message('Ваш запрос удачно отправлен',
                             sender='noreply@demo.com',
                             recipients=[email])
    msg_to_partner.body = f''' Здравствуйте {(name)}

    Запрос на регистрацию вас в качестве партнера сайта rentavtoby.by успешно получен админом!
    Пожалуйста, ожидайте звонка в течение 48 часов.
    Если по каким-либо причинам с вами не свяжутся в срок, пожалуйста позвоните: +37533-602-444-2.
    Cпасибо за оставленную заявку и хорошего настроения!

    C уважением RentAvtoBy Тел: +375336024442
    '''
    mail.send(msg_to_partner)


# Сохранение картинок при запросе сотрудничества
def save_request_pic(form_name, form_surname, form_pic):
    picture_fn = str(form_name) + str(' ') + str(form_surname) + str('_') + form_pic.filename
    picture_path = os.path.join(current_app.root_path, 'static/request_partnership_photos', picture_fn)
    i = Image.open(form_pic)
    i.save(picture_path)
    return picture_fn


# Сохрание 32 страницы паспорта для отправки мне на почту (Удаляется при новом запросе).
def save_request_forsend_pic32(pic):
    picture_fn = str('Page32.jpg')
    picture_path = os.path.join(current_app.root_path, 'static/request_partnership_photos', picture_fn)
    i = Image.open(pic)
    i.save(picture_path)
    return picture_fn


# Сохрание 31 страницы паспорта для отправки мне на почту (Удаляется при новом запросе).
def save_request_forsend_pic31(pic):
    picture_fn = str('Page31.jpg')
    picture_path = os.path.join(current_app.root_path, 'static/request_partnership_photos', picture_fn)
    i = Image.open(pic)
    i.save(picture_path)
    return picture_fn


# Сохрание страницы паспорта с пропиской для отправки мне на почту (Удаляется при новом запросе).
def save_request_forsend_residence(pic):
    picture_fn = str('Residence.jpg')
    picture_path = os.path.join(current_app.root_path, 'static/request_partnership_photos', picture_fn)
    i = Image.open(pic)
    i.save(picture_path)
    return picture_fn


# Отправка сообщений на почту клиента при успешном бронировании
def send_book_approval(voucherint, post_author_name, post_author_surname, post_author_email, post_author_phone, post_car_brand, post_car_engine, post_car_transmission, post_car_fuel, client_name, client_birth, client_email, client_phone, dayprice, totalprice, datefrom, dateto, booktime, post_car_model, sum):
    send_booking = Message(f'''Успешное бронирование номер: {(voucherint)}''',
                             sender='noreply@demo.com',
                             recipients=[client_email])
    send_booking.body = f''' Здравствуйте {(client_name)}

    Бронирование автомобиля {(post_car_brand)} {(post_car_model)} успешно подтверждено!
    Не забудьте заблаговременно согласовать точное место получения автомобиля по телефону: {(post_author_phone)} или по почте: {(post_author_email)}. Или напишите арендодателю в Viber, WhatsApp или Telegram на номер {(post_author_phone)}.

    Информация о бронировании:
    Арендодатель: Имя: {(post_author_name)} {(post_author_surname)}. Контакты: {(post_author_email)}, {(post_author_phone)}(Viber, WhatsApp, Telegram).
    Автомобиль: {(post_car_brand)}, {(post_car_transmission)}. Двигатель: {(post_car_engine)}, {(post_car_fuel)}.

    Арендатор: {(client_name)}, {(client_birth)}, {(client_email)}, {(client_phone)}.

    При получении автомобиля нужно оплатить: {(totalprice)} BYN, (в сутки {(dayprice)} BYN.)
    Получение автомобиля в г. Брест, в {(booktime)} часов, дата {(datefrom)}.
    Возврат автомобиля в г. Брест, до 08:30, дата {(dateto)}.
    Количество дней: {(sum)}

    Для получении авто, покажите арендодателю этот Email или страницу брони с сайта в электронном или распечатанном виде.

    Для изменения или отмены брони, пожалуйста позвоните: {(post_author_phone)}.

    C искренней любовью RentAvtoBy. Тел: 80336024442
    With sincere love RentAvtoBy. Phone: 80336024442
    '''
    mail.send(send_booking)


# Отправка сообщений на почту арендодателя, при успешном бронировании, дублируется на мою почту.
def send_book_to_lessor(voucherint, post_author_name, post_author_surname, post_author_email, post_author_phone, post_car_brand, post_car_engine, post_car_transmission, post_car_fuel, client_name, client_birth, client_email, client_phone, dayprice, totalprice, datefrom, dateto, booktime, post_car_model, post_car_plate, post_id, sum, post_author_id):
    send_to_lessor = Message(f'''Новое бронирование номер: {(voucherint)}''',
                             sender='noreply@demo.com',
                             recipients=[post_author_email, 'neverleash@gmail.com'])
    send_to_lessor.body = f''' Здравствуйте {(post_author_name)}

    Бронирование автомобиля {(post_car_brand)} {(post_car_model)} успешно подтверждено!
    Не забудьте заблаговременно согласовать с арендатором точное место получения автомобиля по телефону: {(client_phone)} или, если клиент не поднимает трубку, написать ему в (Viber, WhatsApp, Telegram) или на почту {(client_email)}.

    Информация о бронировании:
    ID арендодателя: {(post_author_id)}.
    Объявление: {(post_id)}, гос-номер автомобиля {(post_car_plate)}.
    Арендодатель: Имя: {(post_author_name)} {(post_author_surname)}. Контакты: {(post_author_email)}, {(post_author_phone)}(Viber, WhatsApp, Telegram).
    Автомобиль: {(post_car_brand)}, {(post_car_transmission)}. Двигатель: {(post_car_engine)}, {(post_car_fuel)}.

    Арендатор: {(client_name)}, {(client_birth)}, {(client_email)}, {(client_phone)}.

    При получении автомобиля принять оплату: {(totalprice)} BYN, (в сутки {(dayprice)} BYN.)
    Получение автомобиля в г. Брест, в {(booktime)} часов, дата {(datefrom)}.
    Возврат автомобиля в г. Брест, до 08:30, дата {(dateto)}.
    Количество дней: {(sum)}.

    При получении автомобиля арендатором, не забудьте сверить номер брони и подписать договор аренды - только в таком случае вы максимально защищены от рисков.

    C искренней любовью RentAvtoBy. Тел: 80336024442
    With sincere love RentAvtoBy. Phone: 80336024442
    '''
    mail.send(send_to_lessor)
