from flask import render_template, url_for, flash, redirect, request, Blueprint
from flask_login import login_user, current_user, logout_user, login_required
from app import db, bcrypt
from app.models import User, Post, Vouchers
from app.users.forms import (AdminLoginForm, UpdadeAccountForm, RequestResetForm, ResetPasswordForm)
from app.users.utils import save_picture, send_reset_email
import datetime

users = Blueprint('users', __name__)


def add_auth():
    logindate = datetime.datetime.now().strftime("%d.%m.%Y >> %X")
    f = open('auth_list.xrpt', 'a')
    user_login = current_user.username + ' id: ' + str(current_user.id) + '\n' + logindate
    f.write('\n'+user_login)
    f.close()


# Вход на сайт
@users.route('/login', methods=['POST', 'GET'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('users.adminlogged'))
    form = AdminLoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.name.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            add_auth()
            return redirect(url_for('users.adminlogged'))
        else:
            flash('Неправильное имя пользователя или пароль!')
    return render_template('adminlogin.html', form=form)


@users.route('/adminlogged', methods=['GET', 'POST'])
@login_required
def adminlogged():
    form = UpdadeAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Информация успешно изменена!', 'success')
        return redirect(url_for('users.adminlogged'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    image_file = url_for('static', filename='profile pictures/' + current_user.image_file)
    return render_template('adminlogged.html', title='Аккаунт', image_file=image_file, form=form)


# Выход с логина сайта
@users.route('/logout')
def logout():
    # send_del_data = current_user.username
    # del_auth(send_del_data)
    logout_user()
    return redirect(url_for('main.home'))



# Страница "Мои объявления"
@users.route('/user/<string:username>')
def user_posts(username):
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(username=username).first_or_404()
    posts = Post.query.filter_by(author=user)\
        .order_by(Post.date_posted.desc())\
        .paginate(page=page, per_page=5)
    return render_template('user_posts.html', posts=posts, user=user, title=user.username)


@users.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        flash('Письмо с инструкциями по восстановлению было выслано на ваш E-MAIL.', 'info')
        return redirect(url_for('users.login'))
    return render_template('reset_request.html', title='Восстановление пароля', form=form)


@users.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('users.home'))
    user = User.verify_reset_token(token)
    if user is None:
        flash('Ошибка. Невозможно восстановить пароль', 'warning')
        return redirect(url_for('users.reset_request'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        flash('Ваш пароль был успешно изменен!', 'success')
        return redirect(url_for('users.login'))
    return render_template('reset_token.html', title='Reset Password', form=form)


@users.route('/calendarinfo/<int:post_id>', methods=['GET', 'POST'])
@login_required
def calendarinfo(post_id):
    import calendar
    from datetime import timedelta
    current_year = int(datetime.datetime.now().strftime("%Y"))
    a = calendar.LocaleHTMLCalendar(locale='Russian_Russia.1251')
    b = (a.formatyearpage(current_year, width=3).decode('utf8'))
    all_dates = Vouchers.query.filter_by(post=post_id).all()
    return render_template('calendar.html', text = b, all_dates=all_dates)
