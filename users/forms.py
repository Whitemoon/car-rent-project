from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from flask_login import current_user
from app.models import User


# Создание нового пользователя из панели GOD
class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=10, max=20)])
    password = StringField('Пароль', validators=[DataRequired(), Length(min=16)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    name = StringField('Имя', validators=[DataRequired(), Length(min=2)])
    surname = StringField('Фамилия', validators=[DataRequired(), Length(min=2)])
    phone = StringField('Телефон', validators=[DataRequired(), Length(min=13, max=13)])
    submit = SubmitField('Создать')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('Заданное имя уже используется')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('Заданный Email уже используется')

    def validate_phone(self, phone):
        user = User.query.filter_by(phone=phone.data).first()
        if user:
            raise ValidationError('Заданный телефон уже используется')


# Вход в систему для зарегистрированного агента на странице adminlogin.html
class AdminLoginForm(FlaskForm):
    name = StringField('Имя', validators=[DataRequired(), Length(min=10, max=20)])
    password = PasswordField('Пароль', validators=[DataRequired(), Length(min=16)])
    remember = BooleanField('Запомнить меня', default=False)
    submit = SubmitField('Войти в систему')


# Обновление логина и мейла из ЛК пользователя
class UpdadeAccountForm(FlaskForm):
    username = StringField('Имя', validators=[DataRequired(), Length(min=10, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    picture = FileField('Обновить аватарку', validators=[FileAllowed(['jpg', 'png'])])
    submit = SubmitField('Изменить')

    def validate_username(self, username):
        if username.data != current_user.username:
            user = User.query.filter_by(username=username.data).first()
            if user:
                raise ValidationError('Заданное имя уже используется')

    def validate_email(self, email):
        if email.data != current_user.email:
            email = User.query.filter_by(email=email.data).first()
            if email:
                raise ValidationError('Заданный Email уже используется')


# Форма после нажатия на кнопку "Восстановить пароль?" на странице логина.
class RequestResetForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Изменить пароль')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError('Нет аккаунтов с таким Email')


# Изменение пароля после перехода по ссылке из письма
class ResetPasswordForm(FlaskForm):
    password = PasswordField('Новый пароль', validators=[DataRequired(), Length(min=16)])
    confirm_password = PasswordField('Подтвердите новый пароль', validators=[DataRequired(), Length(min=16),  EqualTo('password')])
    submit = SubmitField('Изменить пароль')


# Контактная форма на странице Как Стать Партнером
class ContactForm(FlaskForm):
    cont_name = StringField('Имя', validators=[DataRequired(), Length(min=2, max=20)], render_kw={"placeholder": "Введите ваше имя как в паспорте!"})
    cont_surname = StringField('Фамилия', validators=[DataRequired(), Length(min=3, max=20)],
                               render_kw={"placeholder": "Введите вашу фамилию как в паспорте!"})
    cont_phone = StringField(validators=[DataRequired(), Length(min=9, max=9)],  render_kw={"placeholder": "Телефон (должен быть доступен)"})
    cont_email = StringField('Email', validators=[DataRequired(), Email()], render_kw={"placeholder": "Должен быть актуален!"})
    cont_datas = TextAreaField('Дополнительная информация', render_kw={"placeholder": "Дополнительная информация по необходимости", 'rows': '4'})
    cont_picture32 = FileField('Паспорт: стр 32', validators=[DataRequired(), FileAllowed(['jpg', 'jpeg', 'png', 'pdf', 'webp'])])
    cont_picture31 = FileField('Паспорт: стр 31', validators=[DataRequired(), FileAllowed(['jpg', 'jpeg', 'png', 'pdf', 'webp'])])
    cont_picture_residence = FileField('Паспорт: прописка', validators=[DataRequired(), FileAllowed(['jpg', 'jpeg', 'png', 'pdf', 'webp'])])
    cont_submit = SubmitField('Отправить на рассмотрение')
