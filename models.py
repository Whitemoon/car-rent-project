from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from app import db, login_manager
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


# Добавление пользвоателя со страницы God
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(100), unique=True, nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default='default.jpg')
    password = db.Column(db.String(60), nullable=False)
    name = db.Column(db.String(60), nullable=False)
    surname = db.Column(db.String(60), nullable=False)
    phone = db.Column(db.String(60), unique=True, nullable=False)
    posts = db.relationship('Post', backref='author', lazy=True)

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)


# Добавление нового объявления
class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    car_brand = db.Column(db.String(10), nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    content = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    car_model = db.Column(db.String(10), nullable=False)
    car_transmission = db.Column(db.String(10), nullable=False)
    car_engine = db.Column(db.String(10), nullable=False)
    car_insuerance = db.Column(db.String(10), nullable=False)
    price = db.Column(db.String(10), nullable=False)
    car_color = db.Column(db.String(10), nullable=False)
    car_plate = db.Column(db.String(10), nullable=False)
    car_creation = db.Column(db.String(20), nullable=False)
    car_body = db.Column(db.String(15), nullable=False)
    car_fuel = db.Column(db.String(10), nullable=False)
    car_drive = db.Column(db.String(10), nullable=False)
    notavailable = db.Column(db.Text, default='0  ')
    profilepicture = db.Column(db.String(60), nullable=False)
    profilepicture2 = db.Column(db.String(60), nullable=False)
    profilepicture3 = db.Column(db.String(60), nullable=False)


# Новое бронирование
class Vouchers(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    post = db.Column(db.Integer, nullable=False)
    voucher = db.Column(db.Integer, nullable=False, unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    booktime = db.Column(db.DateTime, nullable=False)
    dayprice = db.Column(db.String(10), nullable=False)
    totalprice = db.Column(db.String(10), nullable=False)
    totaldays = db.Column(db.Integer, nullable=False)
    client_name = db.Column(db.String(40), nullable=False)
    client_birth = db.Column(db.String(20), nullable=False)
    client_email = db.Column(db.String(60), unique=True, nullable=False)
    client_phone = db.Column(db.String(60), unique=True, nullable=False)
    book_from = db.Column(db.String(10), nullable=False)
    book_to = db.Column(db.String(10), nullable=False)
    booktimefrom_to = db.Column(db.String(20), nullable=False)
