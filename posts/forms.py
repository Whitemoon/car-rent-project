from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, SelectField, RadioField, BooleanField
from wtforms.fields.html5 import DateField, TimeField
from wtforms.validators import DataRequired, Length, Email
from flask_wtf.file import FileField, FileAllowed


# Форма подачи нового объявления: post/new
class PostForm(FlaskForm):
    photo_1 = FileField('Главное фото', validators=[DataRequired(), FileAllowed(['jpg', 'jpeg', 'png', 'webp'])])
    photo_2 = FileField('Фото 2', validators=[DataRequired(), FileAllowed(['jpg', 'jpeg', 'png', 'webp'])])
    photo_3 = FileField('Фото 3', validators=[DataRequired(), FileAllowed(['jpg', 'jpeg', 'png', 'webp'])])
    car_brand = StringField('Марка', validators=[DataRequired(), Length(min=3, max=14)])
    car_model = StringField('Модель', validators=[DataRequired(), Length(min=2, max=20)])
    car_color = StringField('Цвет кузова', validators=[DataRequired(), Length(min=3, max=15)])
    car_plate = StringField('Регистрационный номер', validators=[DataRequired(), Length(min=8, max=8)])
    car_creation = StringField('Год выпуска', validators=[DataRequired(), Length(min=4, max=4)])
    car_body = SelectField('Тип кузова', validators=[DataRequired()], choices=[('Седан', 'Седан'),
                           ('Универсал', 'Универсал'),
                           ('Хэтчбек', 'Хэтчбек'),
                           ('Минивен', 'Минивен'),
                           ('Внедорожник', 'Внедорожник'),
                           ('Пикап', 'Пикап'),
                           ('Бус', 'Бус'),
                           ('Кабриолет', 'Кабриолет'),
                           ('Грузовик', 'Грузовик')])
    car_fuel = SelectField('Вид топлива', choices=[('Бензин', 'Бензин'),
                           ('Дизель', 'Дизель'), ('Электро', 'Электричество')])
    car_engine = SelectField('Объем двигателя Л', choices=[('0.6', '0.6'),
                                                           ('0.7', '0.7'),
                                                           ('0.8', '0.8'),
                                                           ('0.9', '0.9'),
                                                           ('1.0', '1.0'),
                                                           ('1', '1 литр'),
                                                           ('1.2', '1.2'),
                                                           ('1.3', '1.3'),
                                                           ('1.4', '1.4'),
                                                           ('1.5', '1.5'),
                                                           ('1.6', '1.6'),
                                                           ('1.7', '1.7'),
                                                           ('1.8', '1.8'),
                                                           ('1.9', '1.9'),
                                                           ('2', '2 литра'),
                                                           ('2.1', '2.1'),
                                                           ('2.2', '2.2'),
                                                           ('2.3', '2.3'),
                                                           ('2.4', '2.4'),
                                                           ('2.5', '2.5'),
                                                           ('2.6', '2.6'),
                                                           ('2.7', '2.7'),
                                                           ('2.8', '2.8'),
                                                           ('2.9', '2.9'),
                                                           ('3', '3 литра'),
                                                           ('3.2', '3.2'),
                                                           ('3.5', '3.5'),
                                                           ('3.6', '3.6'),
                                                           ('4', '4 литра'),
                                                           ('4.5', '4.5'),
                                                           ('5', '5 литров'),
                                                           ('5.5', '5.5'),
                                                           ('6', '6 литров'),
                                                           ('7', '7 литров'),
                                                           ('8', '8 литров'), ])
    car_drive = SelectField('Привод', choices=[('Передний', 'Передний'),
                           ('Задний', 'Задний'), ('Полный', 'Полный')])
    car_transmission = SelectField('Трансмиссия', choices=[('Автомат', 'Автомат'),
                                  ('Ручная', 'Ручная')])
    car_insuerance = SelectField('Вид страховки: ', choices=[('Авто-КАСКО', 'Авто-КАСКО'), ('ОСГО', 'ОСГО')])
    content = TextAreaField('Дополнительное описание (при необходимости)', validators=[Length(max=250)])
    price = StringField('Цена за сутки BYN:', validators=[DataRequired(), Length(min=1, max=6)])
    accept_rules = BooleanField('Я внимательно прочитал и согласился с каждым пунктом публичной оферты.', validators=[DataRequired()])
    submit = SubmitField('Разместить')


# Форма редактирования поданного объявления: post/post_id/update
class UpdatePostForm(FlaskForm):
    car_brand = StringField('Марка', validators=[DataRequired(), Length(min=3, max=14)])
    car_model = StringField('Модель', validators=[DataRequired(), Length(min=2, max=20)])
    car_color = StringField('Цвет кузова', validators=[DataRequired(), Length(min=3, max=15)])
    car_plate = StringField('Регистрационный номер', validators=[DataRequired(), Length(min=8, max=8)])
    car_creation = StringField('Год выпуска', validators=[DataRequired(), Length(min=4, max=4)])
    car_body = SelectField('Тип кузова', choices=[('Седан', 'Седан'),
                           ('Универсал', 'Универсал'),
                           ('Хэтчбек', 'Хэтчбек'),
                           ('Минивен', 'Минивен'),
                           ('Внедорожник', 'Внедорожник'),
                           ('Пикап', 'Пикап'),
                           ('Бус', 'Бус'),
                           ('Кабриолет', 'Кабриолет'),
                           ('Грузовик', 'Грузовик')])
    car_fuel = SelectField('Вид топлива', choices=[('Бензин', 'Бензин'),
                           ('Дизель', 'Дизель'), ('Электро', 'Электричество')])
    car_engine = SelectField('Объем двигателя Л', choices=[('0.6', '0.6'),
                                                           ('0.7', '0.7'),
                                                           ('0.8', '0.8'),
                                                           ('0.9', '0.9'),
                                                           ('1.0', '1.0'),
                                                           ('1', '1 литр'),
                                                           ('1.2', '1.2'),
                                                           ('1.3', '1.3'),
                                                           ('1.4', '1.4'),
                                                           ('1.5', '1.5'),
                                                           ('1.6', '1.6'),
                                                           ('1.7', '1.7'),
                                                           ('1.8', '1.8'),
                                                           ('1.9', '1.9'),
                                                           ('2', '2 литра'),
                                                           ('2.1', '2.1'),
                                                           ('2.2', '2.2'),
                                                           ('2.3', '2.3'),
                                                           ('2.4', '2.4'),
                                                           ('2.5', '2.5'),
                                                           ('2.6', '2.6'),
                                                           ('2.7', '2.7'),
                                                           ('2.8', '2.8'),
                                                           ('2.9', '2.9'),
                                                           ('3', '3 литра'),
                                                           ('3.2', '3.2'),
                                                           ('3.5', '3.5'),
                                                           ('3.6', '3.6'),
                                                           ('4', '4 литра'),
                                                           ('4.5', '4.5'),
                                                           ('5', '5 литров'),
                                                           ('5.5', '5.5'),
                                                           ('6', '6 литров'),
                                                           ('7', '7 литров'),
                                                           ('8', '8 литров'), ])
    car_drive = RadioField('Привод', choices=[('Передний', 'Передний'),
                           ('Задний', 'Задний'), ('Полный', 'Полный')])
    car_transmission = RadioField('Трансмиссия', choices=[('Автомат', 'Автомат'),
                                  ('Ручная', 'Ручная')])
    car_insuerance = RadioField('Вид страховки: ', choices=[('Авто-КАСКО', 'Авто-КАСКО'), ('ОСГО', 'ОСГО')])
    content = TextAreaField('Дополнительное описание (при необходимости)', validators=[Length(max=250)])
    price = StringField('Цена за сутки BYN:', validators=[DataRequired(), Length(min=1, max=6)])
    submit = SubmitField('Применить изменения')


class UpdatePhoto1Form(FlaskForm):
    photo_1 = FileField('Главное фото', validators=[DataRequired(), FileAllowed(['jpg', 'jpeg', 'png', 'webp'])])
    submit = SubmitField('Записать')


class UpdatePhoto2Form(FlaskForm):
    photo_2 = FileField('Фото 2', validators=[DataRequired(), FileAllowed(['jpg', 'jpeg', 'png', 'webp'])])
    submit = SubmitField('Записать')


class UpdatePhoto3Form(FlaskForm):
    photo_3 = FileField('Фото 3', validators=[DataRequired(), FileAllowed(['jpg', 'jpeg', 'png', 'webp'])])
    submit = SubmitField('Записать')


# Выбор дат в календаре
class IndexcalendarForm(FlaskForm):
    datefrom = DateField('DatePicker', format='%Y-%m-%d', validators=[DataRequired()])
    dateto = DateField('DatePicker', format='%Y-%m-%d', validators=[DataRequired()])


# Выбор дат внутри объявления (ChoosecalendarForm)
class PostcalendarForm(FlaskForm):
    datefrom = DateField('DatePicker', format='%Y-%m-%d', validators=[DataRequired()])
    dateto = DateField('DatePicker', format='%Y-%m-%d', validators=[DataRequired()])
    exacttime = TimeField('TimePicker')
    returntime = TimeField('TimePicker')
    client_name = StringField('Имя', validators=[DataRequired(), Length(min=2, max=30)], render_kw={"placeholder": "Иванов Иван"})
    client_birth = StringField(validators=[DataRequired(), Length(min=8, max=10)],  render_kw={"placeholder": "Число в формате 13.01.1992"})
    client_email = StringField('Email', validators=[DataRequired(), Email()], render_kw={"placeholder": "Должен быть актуален!"})
    client_phone = StringField(validators=[DataRequired(), Length(min=9, max=15)],  render_kw={"placeholder": "Телефон (должен быть доступен)"})
