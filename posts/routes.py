from flask import (render_template, url_for, flash,
                   redirect, request, abort, Blueprint, g)
from flask_login import current_user, login_required
from app import db
from app.models import Post, User, Vouchers
from app.posts.forms import PostForm, UpdatePostForm, UpdatePhoto1Form, UpdatePhoto2Form, UpdatePhoto3Form
from app.posts.utils import save_picture, save_picture2, save_picture3
from app.posts.forms import PostcalendarForm
from datetime import datetime, timedelta
import os
from random import randint
from app.users.utils import send_book_approval, send_book_to_lessor
from dateutil.relativedelta import relativedelta


posts = Blueprint('posts', __name__)


@posts.route('/post/new', methods=['GET', 'POST'])
@login_required
def new_post():
    form = PostForm()
    if form.validate_on_submit():
        save_picture(form.photo_1.data, form.car_plate.data)
        save_picture2(form.photo_2.data, form.car_plate.data)
        save_picture3(form.photo_3.data, form.car_plate.data)
        _, f_ext1 = os.path.splitext(form.photo_1.data.filename)
        _, f_ext2 = os.path.splitext(form.photo_2.data.filename)
        _, f_ext3 = os.path.splitext(form.photo_3.data.filename)
        post = Post(car_brand=form.car_brand.data.capitalize(),
                    car_model=form.car_model.data.capitalize(),
                    car_color=form.car_color.data.capitalize(),
                    car_plate=form.car_plate.data,
                    car_creation=form.car_creation.data,
                    car_body=form.car_body.data,
                    car_fuel=form.car_fuel.data,
                    car_engine=form.car_engine.data,
                    car_drive=form.car_drive.data,
                    car_transmission=form.car_transmission.data,
                    car_insuerance=form.car_insuerance.data,
                    price=form.price.data,
                    content=form.content.data.capitalize(),
                    profilepicture=current_user.username + form.car_plate.data + f_ext1,
                    profilepicture2=current_user.username + form.car_plate.data + f_ext2,
                    profilepicture3=current_user.username + form.car_plate.data + f_ext3,
                    author=current_user)
        db.session.add(post)
        db.session.commit()
        flash('Объявление успешно размещено', 'success')
        return redirect(url_for('main.home'))
    return render_template('create_post.html', title='Новое объявление', form=form, legend='Новое объявление:')


@posts.route("/post/<int:post_id>", methods=['GET', 'POST'])
def post(post_id):
    currentdate = datetime.now().strftime("%Y-%m-%d")
    mindateto = datetime.now() + timedelta(days=1)
    mindateto = mindateto.strftime("%Y-%m-%d")
    post = Post.query.get_or_404(post_id)
    form = PostcalendarForm()
    voucherint = randint(0, 100000)
    maxavailabledate = datetime.now() + timedelta(days=366)
    maxavailabledate = maxavailabledate.strftime("%Y-%m-%d")
    sevendaysprice = int(post.price)*7
    if form.validate_on_submit():
        datefrom = form.datefrom.data
        dateto = form.dateto.data
        # Checking if the beggining date does not exceed the end date of the vaucher
        if datefrom > dateto:
            flash('ОШИБКА! Дата начала аренды не может быть больше даты окончания аренды!')
            currentdate = datetime.now().strftime("%Y-%m-%d")
            mindateto = datetime.now() + timedelta(days=1)
            mindateto = mindateto.strftime("%Y-%m-%d")
            return render_template('post.html', title=post.car_brand, post=post, form=form, currentdateset=currentdate, mindateto=mindateto, maxavailabledate=maxavailabledate, sevendaysprice=sevendaysprice)
        if dateto > datefrom:
            notavailable_data = post.notavailable
            if notavailable_data == "":
                f_date = form.datefrom.data.strftime("%Y-%m-%d")
                t_date = form.dateto.data.strftime("%Y-%m-%d")
                f_date = f_date.split('-')
                t_date = t_date.split('-')
                f_dateready = datetime(int(f_date[0]),int(f_date[1]),int(f_date[2]))
                t_dateready = datetime(int(t_date[0]),int(t_date[1]),int(t_date[2]))
                ft_date = t_dateready - f_dateready
                sum=ft_date.days
                sum_voucher=sum*int(post.price)
                dayprice = post.price
                totalprice = sum_voucher
                totaldays = sum
                client_name = form.client_name.data
                client_birth = form.client_birth.data
                client_email = form.client_email.data
                client_phone = form.client_phone.data
                booktime = str(form.exacttime.data)
                voucher_ok = Vouchers(post=post_id, voucher=voucherint, user_id=post.author.id, booktime=datetime.utcnow()+timedelta(hours=3), dayprice=dayprice, totalprice=totalprice, totaldays=totaldays, client_name=client_name, client_birth=client_birth, client_email=client_email, client_phone=client_phone, book_from=datefrom, book_to=dateto, booktimefrom_to=booktime)
                db.session.add(voucher_ok)
                db.session.commit()
                # Below was a code like: notavailable_list=list()
                notavailable_list=str()
                date_generated = [datefrom + timedelta(days=x)
                for x in range(0, (dateto-datefrom).days)]
                for date in date_generated:
                    # Below wos code like: notavailable_list.append(date.strftime("%Y-%m-%d"))
                    notavailable_list += (date.strftime("%Y-%m-%d,"))
                # Below was a code like: notavailable_list += str(dateto)+","
                post.notavailable=notavailable_list
                db.session.commit()
                # The code below sends emails and renders checkout template
                send_book_approval(voucherint, post.author.name, post.author.surname, post.author.email, post.author.phone, post.car_brand, post.car_engine, post.car_transmission, post.car_fuel, client_name, client_birth, client_email, client_phone, dayprice, totalprice, datefrom, dateto, booktime, post.car_model, sum)
                send_book_to_lessor(voucherint, post.author.name, post.author.surname, post.author.email, post.author.phone, post.car_brand, post.car_engine, post.car_transmission, post.car_fuel, client_name, client_birth, client_email, client_phone, dayprice, totalprice, datefrom, dateto, booktime, post.car_model, post.car_plate, post.id, sum, post.author.id)
                return render_template('checkout.html', title='Бронь подтверждена!', form=form, post=post, voucherint=voucherint, sum_voucher=sum_voucher, maxavailabledate=maxavailabledate, sevendaysprice=sevendaysprice)
            else:
                # Здесь нужно сделать проерку на совпадение параметра post.notavailable
                f_date = form.datefrom.data.strftime("%Y-%m-%d")
                t_date = form.dateto.data.strftime("%Y-%m-%d")
                f_date = f_date.split('-')
                t_date = t_date.split('-')
                f_dateready = datetime(int(f_date[0]),int(f_date[1]),int(f_date[2]))
                t_dateready = datetime(int(t_date[0]),int(t_date[1]),int(t_date[2]))
                ft_date = t_dateready - f_dateready
                sum=ft_date.days
                date_generated = [datefrom + timedelta(days=x)
                for x in range(0, (dateto-datefrom).days)]
                check_the_data = list()
                for date in date_generated:
                    check_the_data.append(date.strftime("%Y-%m-%d"))
                #Checking if dates are already taken
                a = check_the_data
                b = post.notavailable.split(',')
                result=list(set(a) & set(b))
                if result:
                    flash('ОШИБКА! На выбранные даты машина уже забронирована!')
                    currentdate = datetime.now().strftime("%Y-%m-%d")
                    mindateto = datetime.now() + timedelta(days=1)
                    mindateto = mindateto.strftime("%Y-%m-%d")
                    return render_template('post.html', title=post.car_brand, post=post, form=form, currentdateset=currentdate, mindateto=mindateto, maxavailabledate=maxavailabledate, sevendaysprice=sevendaysprice)
                else:
                    alldates = b.append(a)
                    sum_voucher=sum*int(post.price)
                    dayprice = post.price
                    totalprice = sum_voucher
                    totaldays = sum
                    client_name = form.client_name.data
                    client_birth = form.client_birth.data
                    client_email = form.client_email.data
                    client_phone = form.client_phone.data
                    booktime = str(form.exacttime.data)
                    voucher_ok = Vouchers(post=post_id, voucher=voucherint, user_id=post.author.id, booktime=datetime.utcnow()+timedelta(hours=3), dayprice=dayprice, totalprice=totalprice, totaldays=totaldays, client_name=client_name, client_birth=client_birth, client_email=client_email, client_phone=client_phone, book_from=datefrom, book_to=dateto, booktimefrom_to=booktime)
                    db.session.add(voucher_ok)
                    db.session.commit()
                    # Below was a code like: notavailable_list=list()
                    notavailable_list=str(notavailable_data)
                    date_generated = [datefrom + timedelta(days=x)
                    for x in range(0, (dateto-datefrom).days)]
                    for date in date_generated:
                        # Below wos code like: notavailable_list.append(date.strftime("%Y-%m-%d"))
                        notavailable_list += (date.strftime("%Y-%m-%d,"))
                    # Below was a code like: notavailable_list += str(dateto)+","
                    post.notavailable=notavailable_list
                    db.session.commit()
                    db.session.commit()
                    # The code below sends emails and renders checkout template
                    send_book_approval(voucherint, post.author.name, post.author.surname, post.author.email, post.author.phone, post.car_brand, post.car_engine, post.car_transmission, post.car_fuel, client_name, client_birth, client_email, client_phone, dayprice, totalprice, datefrom, dateto, booktime, post.car_model, sum)
                    send_book_to_lessor(voucherint, post.author.name, post.author.surname, post.author.email, post.author.phone, post.car_brand, post.car_engine, post.car_transmission, post.car_fuel, client_name, client_birth, client_email, client_phone, dayprice, totalprice, datefrom, dateto, booktime, post.car_model, post.car_plate, post.id, sum, post.author.id)
                    return render_template('checkout.html', title='Бронь подтверждена!', form=form, post=post, voucherint=voucherint, sum_voucher=sum_voucher, maxavailabledate=maxavailabledate, sevendaysprice=sevendaysprice)
    return render_template('post.html', title=post.car_brand, post=post, form=form, currentdateset=currentdate, mindateto=mindateto, maxavailabledate=maxavailabledate, sevendaysprice=sevendaysprice)


# Редактирование объявления.
@posts.route("/post/<int:post_id>/update", methods=['GET', 'POST'])
@login_required
def update_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    form = UpdatePostForm()
    photoform = UpdatePhoto1Form()
    secondphotoform = UpdatePhoto2Form()
    thirdphotoform = UpdatePhoto3Form()
    if form.validate_on_submit():
        post.car_brand=form.car_brand.data.capitalize()
        post.car_model=form.car_model.data.capitalize()
        post.car_color=form.car_color.data.capitalize()
        post.car_plate=form.car_plate.data
        post.car_creation=form.car_creation.data
        post.car_body=form.car_body.data
        post.car_fuel=form.car_fuel.data
        post.car_engine=form.car_engine.data
        post.car_drive=form.car_drive.data
        post.car_transmission=form.car_transmission.data
        post.car_insuerance=form.car_insuerance.data
        post.price=form.price.data
        post.content=form.content.data.capitalize()
        db.session.commit()
        flash('Объявление ID: '+str(post_id)+' отредактированно!', 'success')
        return redirect(url_for('posts.post', post_id=post.id))
    elif photoform.validate_on_submit():
        save_picture(photoform.photo_1.data, post.car_plate)
        form.car_brand.data = post.car_brand
        form.car_model.data = post.car_model
        form.car_color.data = post.car_color
        form.car_plate.data = post.car_plate
        form.car_creation.data = post.car_creation
        form.car_body.data = post.car_body
        form.car_fuel.data = post.car_fuel
        form.car_engine.data = post.car_engine
        form.car_drive.data = post.car_drive
        form.car_transmission.data = post.car_transmission
        form.car_insuerance.data = post.car_insuerance
        form.price.data = post.price
        form.content.data = post.content
        flash('Объявление ID: '+str(post_id)+' изменена основная фотография!', 'success')
        return redirect(url_for('posts.update_post', post_id=post.id))
    elif secondphotoform.validate_on_submit():
        save_picture2(secondphotoform.photo_2.data, post.car_plate)
        form.car_brand.data = post.car_brand
        form.car_model.data = post.car_model
        form.car_color.data = post.car_color
        form.car_plate.data = post.car_plate
        form.car_creation.data = post.car_creation
        form.car_body.data = post.car_body
        form.car_fuel.data = post.car_fuel
        form.car_engine.data = post.car_engine
        form.car_drive.data = post.car_drive
        form.car_transmission.data = post.car_transmission
        form.car_insuerance.data = post.car_insuerance
        form.price.data = post.price
        form.content.data = post.content
        flash('Объявление ID: '+str(post_id)+' изменена вторая фотография!', 'success')
        return redirect(url_for('posts.update_post', post_id=post.id))
    elif thirdphotoform.validate_on_submit():
        save_picture3(thirdphotoform.photo_3.data, post.car_plate)
        form.car_brand.data = post.car_brand
        form.car_model.data = post.car_model
        form.car_color.data = post.car_color
        form.car_plate.data = post.car_plate
        form.car_creation.data = post.car_creation
        form.car_body.data = post.car_body
        form.car_fuel.data = post.car_fuel
        form.car_engine.data = post.car_engine
        form.car_drive.data = post.car_drive
        form.car_transmission.data = post.car_transmission
        form.car_insuerance.data = post.car_insuerance
        form.price.data = post.price
        form.content.data = post.content
        flash('Объявление ID: '+str(post_id)+' изменена третья фотография!', 'success')
        return redirect(url_for('posts.update_post', post_id=post.id))
    elif request.method == 'GET':
        form.car_brand.data = post.car_brand
        form.car_model.data = post.car_model
        form.car_color.data = post.car_color
        form.car_plate.data = post.car_plate
        form.car_creation.data = post.car_creation
        form.car_body.data = post.car_body
        form.car_fuel.data = post.car_fuel
        form.car_engine.data = post.car_engine
        form.car_drive.data = post.car_drive
        form.car_transmission.data = post.car_transmission
        form.car_insuerance.data = post.car_insuerance
        form.price.data = post.price
        form.content.data = post.content
    return render_template('edit_post.html', title='Режим редактирования', form=form, photoform=photoform, secondphotoform=secondphotoform, thirdphotoform=thirdphotoform, legend='ВНИМАНИЕ!!!    🛠    РЕЖИМ РЕДАКТИРОВАНИЯ', post=post)


# Удаление объявления.
@posts.route("/post/<int:post_id>/delete", methods=['GET'])
@login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash('Объявление '+str(post_id)+' удалено!', 'success')
    return redirect(url_for('main.home'))
