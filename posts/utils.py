import os
import secrets
from PIL import Image
from flask_login import current_user
from flask import current_app


# Сохранение основного фото объявления при подаче объявления.
# def save_picture(form_picture):
#     random_hex = secrets.token_hex(8)
#     _, f_ext = os.path.splitext(form_picture.filename)
#     picture_fn = random_hex + str('_') + str(current_user.username) + f_ext
#     picture_path = os.path.join(current_app.root_path, 'static/post pictures', picture_fn)
#     output_size = (1024, 768)
#     i = Image.open(form_picture)
#     i.thumbnail(output_size)
#     i.save(picture_path)
#     return picture_fn


def save_picture(form_picture, plate):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = str(current_user.username) + str(plate) + f_ext
    picture_path = os.path.join(current_app.root_path, 'static/profile pictures', picture_fn)
    output_size = (1024, 768)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)
    return picture_fn


def save_picture2(form_picture, plate):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = str(current_user.username) + str(plate) + f_ext
    picture_path = os.path.join(current_app.root_path, 'static/profile picture2', picture_fn)
    output_size = (1024, 768)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)
    return picture_fn


def save_picture3(form_picture, plate):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = str(current_user.username) + str(plate) + f_ext
    picture_path = os.path.join(current_app.root_path, 'static/profile picture3', picture_fn)
    output_size = (1024, 768)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)
    return picture_fn
